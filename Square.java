/**
 * <b>Класс содержащий объект "Квадрат" и метод по нахождению его площади.</b>
 */
public class Square extends Shape {
    private Point center;
    private double side;

    public Square(Color color, Point center, double side) {
        super(color);
        this.center = center;
        this.side = side;
    }

    @Override
    public String toString() {
        return "Квадрат {Центр " + center + " Сторона: " + side + " Цвет: " + getColor() + "}";
    }

    /**
     * Возвращает площадь квадрата
     *
     * @return площадь квадрата
     */
    @Override
    public double area() {
        return side * side;
    }
}