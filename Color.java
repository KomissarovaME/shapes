/**
 * Возможные варианты цвета фигуры.
 */
public enum Color {
    YELLOW,
    RED,
    GREEN,
    BLUE,
    WHITE
}