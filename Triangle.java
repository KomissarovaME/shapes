/**
 * <b>Класс содержащий объект "Треугольник" и метод по нахождению его площади.</b>
 */
public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle(Color color, Point a, Point b, Point c) {
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public String toString() {
        return "Треугольник {A " + a + " B " + b + " C " + c + " Цвет: " + getColor() + "}";
    }

    /**
     * Возвращает площадь треугольника
     *
     * @return площадь треугольника
     */
    @Override
    public double area() {
        return Math.abs((a.getX() - c.getX()) * (b.getY() - c.getY()) - (a.getY() - c.getY()) * ((b.getX() - c.getY()))) / 2;
    }
}
