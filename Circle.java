/**
 * <b>Класс содержащий объект "Круг" и метод по нахождению его площади.</b>
 */
public class Circle extends Shape {
    private Point center;
    private double radius;

    public Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Круг {Центр " + center + " Радиус: " + radius + " Цвет: " + getColor() + "}";
    }

    /**
     * Возвращает площадь круга
     *
     * @return площадь круга
     */
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }
}
