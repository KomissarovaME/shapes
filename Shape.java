/**
 * <b>Абстрактный класс содержащий объект "Фигура" и её поля.</b>
 */
public abstract class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Shape() {
        this(Color.WHITE);
    }

    public Color getColor() {
        return color;
    }

    public abstract double area();
}