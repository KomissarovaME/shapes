/**
 * <b>Класс для нахождения самой большой фигуры по площади.</b>
 *
 * @author Комиссарова М.Е., 16ИТ18к
 */
public class Demo {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLUE, new Point(), 5);
        Triangle triangle = new Triangle(Color.RED, new Point(0, 0), new Point(2, 5), new Point(1, 4));
        Square square = new Square(Color.GREEN, new Point(6, 9), 4);

        Shape[] shapes = {circle, triangle, square};
        printArrayElements(shapes);

        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с наибольшей площадью: " + maxShape);
        System.out.println("Её площадь составляет: " + maxShape.area());
    }

    /**
     * Возвращает фигуру с наибольшей площадью
     *
     * @param shapes массив геом. фигур
     * @return фигура с наибольшей площадью
     */
    private static Shape maxShapeArea(Shape[] shapes) {
        Shape maxShape = null;
        double maxArea = Double.NEGATIVE_INFINITY;
        for (Shape shape : shapes) {
            double area = shape.area();
            if (area > maxArea) {
                maxArea = area;
                maxShape = shape;
            }
        }
        return maxShape;
    }

    /**
     * Выводит в консоль содержимое массива, состоящего из объектов
     *
     * @param objects массив объектов
     */
    private static void printArrayElements(Object[] objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }
}